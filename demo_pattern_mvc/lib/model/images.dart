class Images{
  int _id;
  String _url;

  int get id => _id;
  set id(int i){
    _id = i;
  }

  String get url => _url;
  set url(String u){
    _url = u;
  }

  Images(this._id, this._url);

  factory Images.fromJson(Map<String, dynamic> json){
    return Images(int.parse(json['Id']), json['Url']);
  }

  Map<String, dynamic> toJson(){
    var map = Map<String, dynamic>();
    map['id'] = _id;
    map['url'] = _url;

    return map;
  }
}