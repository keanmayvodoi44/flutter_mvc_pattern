class Users{
  String _account;
  String _password;

  String get account => _account;
  set account(String acc){
    _account = acc;
  }

  String get password => _password;
  set password(String pass){
    _password = pass;
  }

  Users(this._account, this._password);

  factory Users.fromJson(Map<String, dynamic> json){
    return Users(json['Account'], json['Password']);
  }

  Map<String, dynamic> toJson(){
    var map = Map<String, dynamic>();
    map['account'] = _account;
    map['password'] = _password;

    return map;
  }
}