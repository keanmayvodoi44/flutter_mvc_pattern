import 'dart:convert';

import 'package:demo_pattern_mvc/model/images.dart';
import 'package:demo_pattern_mvc/utils/network_util.dart';

class HomeController{

  NetworkUtil _netUtil = new NetworkUtil();

  static final URL_GETIMAGES = "getImages.php";

  Future<List<Images>> fetchImages(){
    return _netUtil.get(URL_GETIMAGES).then((res) async {
      final mapResponse = json.decode(res).cast<Map<String, dynamic>>();
      List<Images> listImages = await mapResponse.map<Images>((json){
        return Images.fromJson(json);
      }).toList();
      return listImages;
    }).catchError((onError){
      throw Exception(onError);
    });
  }
}