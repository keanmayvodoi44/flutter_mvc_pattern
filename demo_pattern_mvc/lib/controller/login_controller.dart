import 'package:demo_pattern_mvc/utils/network_util.dart';
import 'package:demo_pattern_mvc/view/home.dart';
import 'package:flutter/material.dart';

class LoginController {

  NetworkUtil _netUtil = new NetworkUtil();

  static final LOGIN_URL = "login.php";

  void fieldFocusChange(BuildContext context, FocusNode present, FocusNode after){
    present.unfocus();
    FocusScope.of(context).requestFocus(after);
  }

  void showSnackbar(GlobalKey<ScaffoldState> scaffoldState, String content){
    final snackBar = new SnackBar(
      content: new Text(
        content,
        style: TextStyle(
            color: Colors.white
        ),
      ),
      duration: Duration(
          seconds: 4
      ),
      backgroundColor: Colors.blue,
    );
    scaffoldState.currentState.showSnackBar(snackBar);
  }

  Future<int> login(BuildContext context, String account, String password) {
    return _netUtil.post(LOGIN_URL, body: {
      'account': account,
      'password': password
    }).then((res){
      print(res);
      if(res == 'failt'){
        return -1;
      }
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Home()
        )
      );
    }).catchError((onError){
      throw Exception("Error while fetching data");
    });
  }
}