import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'utility.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  Future<String> get(String url) async {
    http.Response response = await http.get(BASE_URL + url);
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return response.body;
  }

  Future<String> post(String url, {Map headers, body, encoding}) async {
    http.Response response = await http.post(BASE_URL + url, body: body, headers: headers, encoding: encoding);
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return response.body;
  }
}