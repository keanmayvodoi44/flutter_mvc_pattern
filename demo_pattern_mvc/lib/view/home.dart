import 'package:demo_pattern_mvc/controller/home_controller.dart';
import 'package:demo_pattern_mvc/model/images.dart';
import 'package:flutter/material.dart';
import 'package:demo_pattern_mvc/utils/utility.dart';

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<Home>{

  HomeController controller = HomeController();

  bool isLoading;

  List<Images> images;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading = true;
    controller.fetchImages().then((data){
      images = data;
      setState(() {
        isLoading = false;
      });
    }).catchError((onError){
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  Widget _buildAppBar(){
    return AppBar(
      title: Text("Trang chính"),
      centerTitle: true,
    );
  }

  Widget _buildBody(){
    return isLoading?_buildProgressDialog():_buildGridView();
  }

  Widget _buildProgressDialog(){
    return Opacity(
      opacity: 0.5,
      child: Container(
        color: Colors.grey,
        height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _buildGridView(){
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: _buildGridTile(images.length),
      ),
    );
  }

  List<Widget> _buildGridTile(int length){
    List<Container> containers = List<Container>.generate(length, (index){
      return Container(
        padding: EdgeInsets.all(5.0),
        child: FadeInImage.assetNetwork(
          image: BASE_URL + images[index].url,
          placeholder: 'assets/loading.gif',
          fit: BoxFit.fill,
        )
      );
    });
    return containers;
  }
}